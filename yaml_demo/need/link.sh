#!/bin/bash
host="192.168.136.131"
user="root"
pw="111111"
ip=$1
#usage="df / | awk '$5=="/"{sub("%","",$4);print $4}'"
#if [ $usage -gt 80 ]; then
#    LC_ALL=C ifconfig|grep "inet"|grep -v "127.0.0.1"|cut -d: -f2|awk '{print $2}'
#fi
/usr/bin/expect <<-EOF
set timeout -1

spawn ssh $user@$host
expect {

"yes/no" { send "yes\r"; exp_continue}

"$user@$host's password:" { send "$pw\r";exp_continue}
"]# " { send "echo $ip >> /home/dcos/text\r"}
}

expect "]# " 
send "echo $ip >> /home/dcos/text\r"
interact
EOF


