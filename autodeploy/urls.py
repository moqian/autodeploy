# -*- coding: utf-8 -*-
"""autodeploy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import django
from django.conf.urls import include, url
from django.contrib import admin
from django.views.static import serve

from autodeploy import settings
from autodeploy.apps.index.views import IndexView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^index/', include('autodeploy.apps.index.urls')),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^user/', include('autodeploy.apps.user.urls')),
    url(r'^server/', include('autodeploy.apps.server.urls')),
    url(r'^playbooks/', include('autodeploy.apps.playbooks.urls')),
    url(r'^operation/', include('autodeploy.apps.operation.urls')),
    url(r'^upload/', include('autodeploy.apps.common.upload_file.urls')),
]
# 加upload访问的配置
urlpatterns += [url(r'^upload/(?P<path>.*)$', django.views.static.serve,
                    {'document_root': settings.MEDIA_ROOT})]
if not settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT, }),
    ]
