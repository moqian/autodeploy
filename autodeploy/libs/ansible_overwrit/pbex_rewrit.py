# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-6-23 下午4:17.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)
from ansible import constants as C
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.utils.ssh_functions import check_for_controlpersist

# 调用自定义Inventory
# from ansible_overwrit.inventory_overwrit import YunweiInventory as Inventory
try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    
    display = Display()


class YunweiPlaybookExecutor(PlaybookExecutor):
    '''重写PlayBookExecutor'''
    
    def __init__(self, playbooks, inventory, variable_manager, loader, options, passwords, stdout_callback=None):
        self._playbooks = playbooks
        self._inventory = inventory
        self._variable_manager = variable_manager
        self._loader = loader
        self._options = options
        self.passwords = passwords
        self._unreachable_hosts = dict()
        
        if options.listhosts or options.listtasks or options.listtags or options.syntax:
            self._tqm = None
        else:
            self._tqm = TaskQueueManager(inventory=inventory, variable_manager=variable_manager, loader=loader,
                                         options=options, passwords=self.passwords, stdout_callback=stdout_callback)
        
        # Note: We run this here to cache whether the default ansible ssh
        # executable supports control persist.  Sometime in the future we may
        # need to enhance this to check that ansible_ssh_executable specified
        # in inventory is also cached.  We can't do this caching at the point
        # where it is used (in task_executor) because that is post-fork and
        # therefore would be discarded after every task.
        check_for_controlpersist(C.ANSIBLE_SSH_EXECUTABLE)
