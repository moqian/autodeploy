$(function () {
	for (i = 0; i < $(".imginput").length; i++) {
		var $inputi = $(".imginput").eq(i);
		var imgdata = $inputi.attr("upimginfo").split(",");

		//给上传图片的input框加按钮
//		$inputi.after("<div style='display:inline;'>" +
//				          "<span class='graybtn'>" +
//				              "<a href='javascript:;' onclick=\"upload_iframe(this,'"+imgdata[0]+"',"+imgdata[1]+","+imgdata[2]+")\">选择图片</a>"+
//				          "</span>" +
//				          "<span name='' class='upimg'></span>" +
//				      "</div>");
		$inputi.after('<div style="display:inline;"><a class="btn btn-default" onclick="upload_iframe(this,\'' + imgdata[0] + '\',' + imgdata[1] + ',' + imgdata[2] + ')">'
			+ '<span class="glyphicon glyphicon-upload" aria-hidden="true"></span> 上传图片'
			+ '</a>&nbsp;<a class="btn btn-default" onclick="delete_img(this)">清除图片</a>'
			+ '<span name="" class="upimg"></span></div><div class="preview_img"></div>'
		);
//		if($inputi.val().indexOf("upload")!=-1){
//			$inputi.next("div").append("<img height='100' src='"+$(".imginput").eq(i).val()+"' />");
//		}
		var imghtml = "";
		if (imgdata[1] == 0 || imgdata[2] == 0) {
			imghtml = "<img width='100px' src='" + img(100, 100, "image") + "' />"
		} else {
			imghtml = "<img width='100px' src='" + img(imgdata[1], imgdata[2]) + "' />"
		}
		show_img($inputi.parent().find(".preview_img"), $inputi.val(), imghtml);
	}
	set_imginput();
	
	
});
function delete_img(_this) {
	$(_this).next().text('');
	$(_this).parent().parent().find('input').val('');
	$(_this).parent().next().find('img').attr({src: img(100, 100, 'image'), style: ''});
	
	
}
//在.imginput后面显示图片img
function show_img($preview_img, imgsrc, imghtml) {
	var imgsrc = arguments[1] ? arguments[1] : "";
	var imghtml = arguments[2] ? arguments[2] : "";
//	for(i=0;i<$(".imginput").length;i++){
//		var $inputi = $(".imginput").eq(i);
//		var $img = $inputi.next("div").find("img");
	if (imgsrc != "") {
		$preview_img.html("<br/><img width='100px' src='" + imgsrc + "?2014' />");
	} else {
		$preview_img.html(imghtml);
	}
		
//		if(imghtml!=""){
////			$("#preview_img").html(imghtml);
//			$("#preview_img").attr("onerror",imghtml);
//		}
//		else{
//			var $img = $inputi.next("div").find("img");
//			$("#preview_img").html("<br/><img height='100' src='"+$inputi.val()+"?2014' />");
//		}
//		else if($inputi.val()){
//			var $img = $inputi.next("div").find("img");
//			if($img.attr("src")){
//				$img.attr("src",$inputi.val()+"?2014")
//			}else{
//				$inputi.next("div").append("<br/><img height='100' src='"+$inputi.val()+"?2014' />");
//			}
//		}
//	}
}

//把input框的值给span显示用
function set_imginput() {
	for (i = 0; i < $(".imginput").length; i++) {
		var $inputi = $(".imginput").eq(i);
		var val = $inputi.val();
//		var txt;
//		if(val.indexOf("upload")!=-1){
//			txt = val.split("/").reverse()[0]
//		}else{
//			txt = ""
//		}
		$inputi.next("div").find(".upimg").text(val);
	}
}

//改用七牛
var upurl = "/upload_file/upload_image/";//上传的url
var cuturl = "/upload_file/uploadify_script/";//剪裁的url
var delurl = "/upload_file/delete_uploadfile/";//删除的url

/*
 var index;//弹出框的index
 //确定按钮操作
 function doyes(){
 var cc = layer.getChildFrame('.showimg', index);
 alert(JSON.stringify(cc))
 var all_name = layer.getChildFrame('.showimg', index).attr("src")
 alert(all_name)
 var data_name = all_name.split("/")
 //给父页面创建的input框赋值,先判断有没值,有的删除图片
 exname = $(_this).parent().parent().parent().find(".imginput").val();
 if(exname.indexOf("upload")!=-1){
 $.post(delurl, { delete_file: exname } );//删除之
 }
 $(_this).parent().parent().parent().find(".imginput").val(all_name);
 //<img class='upimg' style='display:none;' src='' />
 //$(_this).parent().parent().find(".upimg").attr("src",all_name).show();
 $(_this).parent().parent().find(".upimg").text(data_name[3])
 //	$(_this).parent().parent().find("img").attr("src",all_name+"?2014")//有img标签的显示图片

 //提交剪裁的参数
 var cut_size = layer.getChildFrame('#cut_size', index).val();
 $.post(cuturl, {cut_size:cut_size,file_name:all_name});

 layer.close(index);//关闭层
 }
 //取消按钮操作
 function dono(){
 var all_name = layer.getChildFrame('#showimg', index).attr("src")//如:/upload/upimg...
 alert(all_name)
 $.post(delurl, { delete_file: all_name } );
 layer.close(index);//关闭层
 }
 */
/**
 * 显示弹出框跳转上传图片页
 * _this:既htmlthis节点及元素
 folder:上传的文件夹名,字符串
 width:宽px,int
 height:高px,int
 * */
function upload_iframe(_this, folder, width, height, url) {

	//默认传参
	var folder = arguments[1] ? arguments[1] : 'upimg';
	var width = arguments[2] ? arguments[2] : 0;
	var height = arguments[3] ? arguments[3] : 0;
	var url = arguments[4] ? arguments[4] : '';

	sessionStorage.setItem('folder', folder);
	sessionStorage.setItem('width', width);
	sessionStorage.setItem('height', height);
//	upurl2 = upurl+folder+"/"+width+"/"+height+"/";
	/*以下tab弹出框*/
	//一个子页面
	var tabIframe = function (src, i) {
		return '<iframe name="frame' + i + '" frameborder="0" src="' + src + '" style="width:900px; height:550px;"></iframe>'
//			+'<span class="xubox_botton"><a onclick="doyes();" class="xubox_botton2" href="javascript:;">确认</a>'
//			+'<a onclick="dono();" class="xubox_botton3" href="javascript:;">取消</a></span>';
	};
//	index = $.layer({
//		type: 1,
//		title: '本地图片',
//		maxmin: true,
//		area: ['800px', '600px'],
//		border: [0],
//		shadeClose: false,
//		shift: 'bottom',//底部弹出
//		closeBtn: false,
//		fix : false,
//		move: '.xubox_tabmove',
//		btns: 2,
//		btn: ['确定', '取消'],
//		page: {url:upurl},
//		
//	});
	//之前的弹出框
	//确定按钮回调
	var yes = function uploadyes(index) {
		var $f1 = $(window.frames["frame1"].document);
		if (Number(height)) {
			$f1.find("#cropbtn").click();//调剪裁方法
		}
		var imgurl = $f1.find("#cropurl").text();//拿到剪裁后的图片链接
//		alert(imgurl);
//		var all_name = $f1.find("#showimg").attr("src");//find("#id_span_msg").text();
//		var preview_img_src = $f1.find(".preview-container img").attr("src");
//		var preview_img_html = $f1.find("#preview-pane").html();//find("#id_span_msg").text();
//		var data_name = all_name.split("/")
//		//给父页面创建的input框赋值,先判断有没值,有的删除图片
//		exname = $(_this).parent().parent().parent().find(".imginput").val();
//		if(exname.indexOf("upload")!=-1){
//			$.post(delurl, { delete_file: exname } );//删除之
//		}
		$(_this).parent().parent().parent().find(".imginput").val(imgurl);
		show_img($(_this).parent().parent().find(".preview_img"), imgurl, '');
//		if(preview_img_src==""){
//		}else{
//			show_img($(_this).parent().parent().find(".preview_img"),"",preview_img_html);
//		}
//		
		$(_this).parent().parent().find(".upimg").text(imgurl)
//		
//		//提交剪裁的参数
//		var cut_size = $(window.frames["frame1"].document).find('#cut_size').val();
//		$.post(cuturl, {cut_size:cut_size,file_name:all_name});
		layer.close(index);//关闭层
	}
	var cancel = function uploadno(index) {
		layer.close(index);//关闭层
//		var all_name = $(window.frames["frame1"].document).find("#showimg").attr("src");
//		$.post(delurl, { delete_file: all_name } ); 
	}
	//返回index关闭弹出层用
	iframeurl({
		title: '本地图片',
		area: ['900px', '650px'],
		content: tabIframe(upurl, 1),
		yes: yes,
		cancel: cancel,
		type: 1
	})
//	var index = ly.open({
//		area: ['800px', '600px'],
//		title: '本地图片', 
////		tab: [{
////		}],
//		content: tabIframe(upurl,1),
//		yes: yes,
//		cancel: cancel,
//		btn:['确认','取消']
//	});
	/*
	 //	iframeurl('选择图片', url, area=[600,350], btns=2, yes=yes, no=no, close=close, end=end)
	 var index = $.layer({
	 type : 2,
	 title: '选择图片',//默认显示:信息
	 shift: 'bottom',//底部弹出
	 //        fadeIn: 300,//渐显
	 shadeClose: false,
	 closeBtn: false,
	 btns:2,
	 btn:["确认","取消"],
	 maxmin: true,
	 fix : false,
	 offset: ['',''],//显示位置
	 area: [800,550],//[宽,高],px
	 iframe: {src : upurl2},
	 yes: yes,
	 no: no,
	 });
	 */
}
/**

 tab层
 a: 参数a需要:area,yes,no,data[{title,content},{}]
 */


//layer.tab2 = function(a) {
//    a = a || {};
//    var b = a.tab || {};
//    return layer.open($.extend({
//    	yes:a.yes,
//        cancel:a.cancel,
//        type: 1,
//        btn: ['确定', '取消'],
//        btns: 2,
//        skin: "layui-layer-tab",
//        title: function() {
//            var a = b.length,
//            c = 1,
//            d = "";
//            if (a > 0) for (d = '<span class="layui-layer-tabnow">' + b[0].title + "</span>"; a > c; c++) d += "<span>" + b[c].title + "</span>";
//            return d
//        } (),
//        content: '<ul class="layui-layer-tabmain">' +
//        function() {
//            var a = b.length,
//            c = 1,
//            d = "";
//            if (a > 0) for (d = '<li class="layui-layer-tabli xubox_tab_layer">' + (b[0].content || "no content") + "</li>"; a > c; c++) d += '<li class="layui-layer-tabli">' + (b[c].content || "no  content") + "</li>";
//            return d
//        } () + "</ul>",
//        success: function(a) {
//            var b = a.find(".layui-layer-title").children(),
//            c = a.find(".layui-layer-tabmain").children();
//            b.on("mousedown",
//            function(a) {
//                a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0;
//                var b = $(this),
//                d = b.index();
//                b.addClass("layui-layer-tabnow").siblings().removeClass("layui-layer-tabnow"),
//                c.eq(d).show().siblings().hide()
//            })
//        }
//    },
//    a))
//};