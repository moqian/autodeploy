var map, opts, point, infoWindow, marker, myGeo;
$(function () {
    //获取地址的值给map赋值
    $("#searchpoint input").val($("#id_address").val());
    var coordinate = $("#id_coordinate").val();
    if (coordinate == "") {
        coordinate = "30.271118,120.144872";//给个杭州西湖的坐标
    }
    data = coordinate.split(",")
    map = new BMap.Map("container"); // 创建地图实例
    point = new BMap.Point(data[1], data[0]); // 创建点坐标
    map.centerAndZoom(point, 15);  // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom();    //启用滚轮放大缩小，默认禁用
    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用

    //添加缩放控件
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());
//	opts = { width: 200, height: 50, title: "陌仟科技有限公司" }; //信息窗口  
//	infoWindow = new BMap.InfoWindow("<div style='color:red;'>Tel:15888888888</div>", opts); 

    //点击地址框,弹出框地图
    $("#id_coordinate").on("click", function () {
        $("#searchpoint input").val($("#id_address").val());
        $(".bg").show()
        $(".bg").next("div").show()
    });
});
//点击阴影部分取消弹出框
function hideDiv() {
    $(".bg").hide()
    $(".bg").next("div").hide()
    if (marker != null) {
        map.removeOverlay(marker);//移除标注
//		marker.dispose();
    }
}
function getPointValue() {
    //给地址,经纬度赋值
    $("#id_coordinate").val(coordinate);
//	$("#id_address").val(address);

    $(".bg").hide()
    $(".bg").next("div").hide()
    if (marker != null) {
        map.removeOverlay(marker);//移除标注
//		marker.dispose();
    }
}
var address, coordinate;
//地址解析的函数
function fun_geocoder_getPoint() {
    //创建地址解析的实例
    myGeo = new BMap.Geocoder();
    var value_address_1 = document.getElementById("address_1").value;
    myGeo.getPoint(value_address_1, function (point) {
        if (marker != null) {
            map.removeOverlay(marker);//移除标注
//			marker.dispose();
        }
        if (point) {
            coordinate = point.lat + "," + point.lng;
//			document.getElementById("id_coordinate").value = point.lat + "," + point.lng;
            marker = new BMap.Marker(point);	// 创建标注
            map.centerAndZoom(point, 15);	// 初始化地图，设置中心点坐标和地图级别
            map.addOverlay(marker);
            marker.enableDragging();
            //标注拖拽后的位置
            marker.addEventListener("dragend", function (e) {
                //alert("当前位置：" + e.point.lng + ", " + e.point.lat);
                coordinate = e.point.lat + "," + e.point.lng;
//				$("#id_coordinate").val(e.point.lat + "," + e.point.lng);
                var pt = e.point;
                myGeo.getLocation(pt, function (rs) {
                    var addComp = rs.addressComponents;
                    //给地址框赋值
                    address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
//					$("#id_address").val(address);
                    $("#searchpoint input").val(address);
                });
            });
            //点击的位置
            marker.addEventListener("click", function (e) {
//				alert("当前位置：" + e.point.lat + ", " + e.point.lng);  
                var pointClick = new BMap.Point(e.point.lng, e.point.lat);
                marker.openInfoWindow(infoWindow, pointClick);
            });
        }
    }, "全国");
}
