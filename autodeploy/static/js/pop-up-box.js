$(function () {
    defaultSkin = 'layer-ext-myskin';//自己修改的默认皮肤
    showSkin();
});
/**
 * 可选参数用options字典进行传参,{key:val, ..}
 * 可以不用在意参数顺序,key写对即可
 * 如 showSkin({skin:'***', ..})
 * */
//初始化皮肤
function showSkin(options) {
    options = $.extend({
        skin: defaultSkin,
        extend: ['skin/layer.css'],
    }, options || {});

    if (parent.layer) {
        var layer = parent.layer
    } else {
        var layer = layer
    }
    layer.config({
        skin: options["skin"],
        extend: options["extend"],
        shift: 5,
    });
};

//弹出框,开启功能
function showAlertOn(_this, url, msg) {
    layer.confirm(msg, function () {
        location.href = url;
    }, function (index) {
        $(_this).bootstrapSwitch('state', false);
        layer.close();
    });
}


//弹出框,失败信息/成功信息,8:失败图标(默认) 9:成功图标
function showAlertMsg(msg, icon) {
    var icon = arguments[1] ? arguments[1] : 8;
    layer.alert(msg, {icon: icon})
}

//显示正在提交的弹出框
function showOnsubmit(style) {
    layer.load(style, {time: 1000});
}
/**
 *
 * 紫页面上,获取当前窗口索引
 var index = parent.layer.getFrameIndex(window.name);
 关闭:parent.layer.close(index);
 //让层自适应iframe
 parent.layer.iframeAuto(index);//页面加载完,执行这
 *  title, 标题
 *  url, 链接
 *  area=[1200,600], 页面大小(可选)
 *  */
function iframeurl(options) {
    //默认传参修改
    options = $.extend({
        type: 2,
        title: false,
        area: ['900px', '600px'],
        shadeClose: true,
        content: "",
        success: function () {
        },
        yes: function () {
        },
        cancel: function () {
        },
        end: function () {
        },
        btn: ["确认", "取消"],
        closeBtn: 1,
        offset: 'auto',
        maxmin: false,
        fix: false,
        shade: [0.5, '#000'],
    }, options || {});

    if (parent.layer) {
        var layer = parent.layer
    } else {
        var layer = layer
    }
    var index = layer.open(options);
}
/**
 * 弹出页面窗口
 * 自定义参数：title area btns btn src success yes no close end
 * */
function showIframe(options) {
    options = $.extend({
        type: 2,//窗口类型 2表示页面窗口
        title: false,
        maxmin: false,//窗口最小化/全屏/还原按钮。 
        area: ['900px', '600px'],
        content: "",
        success: function () {
        },
        yes: function () {
        },
        cancel: function () {
        },
        end: function () {
        },
        offset: 'auto',
        btn: ["确认", "取消"],//按钮文本
        closeBtn: 1,//窗口关闭按钮
        shade: [0.5, '#000'],//背景遮罩
        shadeClose: true,
        maxWidth: 400,//当area宽度设为auto时才有用。
        fix: false,//设置窗口是否滚动
        move: false,//设定某个元素来实现对层的拖拽
        moveOut: false,//用于控制层是否允许被拖出可视窗口外
        moveType: 0,//默认为引导式拖动层，若值设为1，则直接拖动层
    }, options || {});

    var index = layer.open(options);
    return index;
}
//弹出输入框 
//支持普通文本框（0）、密码框（1）、文件框（2）、多行文本框（3）, 
function showInput(options) {
    options = $.extend({
        skin: defaultSkin,
        formType: 0,
        title: '',
        value: '',
        maxlength: 200,
        yes: function () {
        },
    }, options || {});
    layer.prompt({
        skin: options["skin"],
        formType: options["formType"],
        title: options["title"],
        value: options["value"],
        maxlength: options["maxlength"],
    }, options["yes"]);
}

//弹出对话框
function showYesorNo(msg, yes, cancel) {
    layer.confirm(msg ? msg : '', yes ? yes : null, cancel ? cancel : function (index) {
        layer.close(index);
    });
}

/** 显示提示信息
 =======
 /**
 * msg:  提示内容
 * shade 控制遮罩，可写[0.5, '#000']
 * time: 自动关闭秒数（默认2）
 * rate:用于控制动画弹出 有七种选择：左上(left-top),上(top), 右上(right-top),
 *        右下(right-bottom),下(bottom),左下(left-bottom),左('left')。
 * end:弹出框关闭时回调
 * */
function showMsg(options) {
    options = $.extend({
        icon: 6,
        msg: "",
        time: 2000,
        shade: false,
        rate: '',
        end: function () {
        },
    }, options || {});

    return layer.msg(options["msg"], {
        icon: options["icon"],
        time: options["time"],
        shade: options["shade"], //是否开启遮罩(默认true)
        rate: options["rate"]
    }, options["end"])
}


/**
 * 传json
 * 弹出普通层:自定义层内内容
 * icon:图标
 * title:弹出框标题，false不显示
 * area:弹出层大小
 * btn:按钮文字，[]
 * content:自定义页面内容,支持html
 * shadeClose：开启遮罩关闭
 * yes:成功方法
 * */
function showCustom(options) {
    options = $.extend({
        icon: 0,
        title: false,
        area: ["500px", "300px"],
        closeBtn: 2,
        btn: ["确定"],
        shadeClose: false,
        content: "",
        yes: function () {
        },
    }, options || {});

    layer.open(options);
}
/**
 * 传json
 * tabs弹出层
 * area:弹出层大小
 * data:所有tab的标题、内容,支持html传入
 * data格式[{
        title: 'TAB1', 
        content: '内容1'
    }, {
        title: 'TAB2', 
        content: '内容2'
    }, {
        title: 'TAB3', 
        content: '内容3'
    }]
 * */
function showTabs(options) {
    options = $.extend({
        area: ["500px", "300px"],
        data: "",
    }, options || {});
    layer.tab({
        skin: defaultSkin,
        area: options["area"],
        tab: options["data"]
    });
}
/**
 * tips显示框
 * _this, 既html onclick里的this,节点元素级this
 * msg, 显示是信息
 * guide: tips框方向,0:上 , 1:左, 2:下,  3:右
 *            4个方位对应四种显示框样式,具体在哪方位与页面节点之类的有影响
 * time, 秒
 * 如onclick="showTips(this,'hhh',0,2)"
 * */
function showTips(_this, msg, guide, time) {
    if (guide == 1) {
        color = '#F26C4F';
    } else if (guide == 3) {
        color = '#c00';
    } else {
        color = '#3595CC';
    }
    layer.tips(msg, _this, {
        tips: [guide, color],
        time: time,
    });
}

/**
 * 确认提示框
 * msg:提示内容,如你确定要删除吗？
 * url:点击确定后跳转的页面url
 * */
function showConfirm(msg, url) {
    layer.confirm(msg, function () {
        location.href = url;
    });
}
