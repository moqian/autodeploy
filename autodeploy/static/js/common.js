$(document).on("click", ".deletebtn", function () {
//	var id = $(this).attr('deleteid');
//	var deleteUrl = $(this).attr('deleteurl') + id + '/';
    var deleteUrl = $(this).attr('deleteurl');
    ajaxTableDelete(deleteUrl, this);
});

//通用table排列数据通过Ajax删除
function ajaxTableDelete(_url, _this) {
    var index = layer.confirm('你确认要删除吗?', function () {
        ajaxDel(_url, _this, index);
    });
}
//访问服务器删除
function ajaxDel(_url, _this, index) {
    layer.close(index)
    $.ajax({
        type: "GET",
        url: (_url ? _url : ""),
        dataType: "json",
        data: "",//{"id":2,"name":"zhangshang"}
//        cache: false,//不缓存
        success: function (msg) {
            if (msg == 1) {
                var $deletedom = $(_this).parents(".deletedom")
                if ($deletedom.html()) {
                    $deletedom.remove();
                } else if($(_this).parents("tr").html()){
                    $(_this).parents("tr").remove();
                }else{
                    $(_this).parents(".gwd-item").remove();
                    location.reload();
                    //$('.kong').show();
                }
            }
            else if (msg == 0) {
                showAlertMsg("删除失败")
            }
        },
        error: function (msg) {
            showAlertMsg("不能连接服务器.")
        }
    });
}

/** 判断图片的大小, ele:节点元素级this, size:图片大小如2表示既2M*/
function filesize(ele, size) {
    // 返回 KB，保留小数点后两位
    var kb = (ele.files[0].size / 1024).toFixed(2)
    var limit = size * 1024;
    if (limit < kb) {
        showAlertMsg('图片大小需小于' + size + 'M')
        $(ele).val("");
    }
}
/** table排序功能,访问服务器的
 * 页面要include head1.html或引入此js文件
 * 调用方法:如TableSorter(["tb1", 0, 1, 2, 3, 6]);
 * 说明:tb1:需排序的table的id,数字:排序的列
 * python代码里用问号传参
 * tablesorter = request.GET.get('tablesorter','null')
 #根据传参得到按排序的那列名
 row = get对应表Row(tablesorter)
 .order_by(row)
 *  */
function TableSorter(args) {
    var $thd = $(args[0] + " thead tr th");
    if (args.length > 1) {
        //排序点击变色SortAscCss
        //当前链接后加传参
        var url = window.location.href;
        var rownum = Number(url.split("tablesorter=")[1]);
        //排序的箭头
        var rm = ".down";
        if (rownum < 0) {
            rownum = rownum * -1;
            rm = ".up";
        }
        for (i = 1; i < args.length; i++) {
            //给那行td加样式,鼠标经过有效果
            $thd.eq(args[i]).css("cursor", "pointer");
            $thd.eq(args[i]).children("span").remove();
            $thd.eq(args[i]).append(" <span class='up'>↑</span><span class='down'>↓</span>");
            if (args[i] == rownum) {
                //那行的td加颜色
                $thd.eq(rownum).addClass("text-success");
                $thd.eq(rownum).children(rm).remove();
            }
        }
        //点击table排序传参
        $thd.click(function () {
            var val = $(this).index();
            //按那几列排序
            for (i = 1; i < args.length; i++) {
                if (args[i] == val) {
                    //判断升序降序排序,点一下升序,在点下降序
                    if (url.indexOf("tablesorter=" + val) >= 0) {
                        val = "-" + val
                    }
                    var data = url.split('&tablesorter');
                    var data = data[0].split('?tablesorter');
                    //拼接链接
                    if (data[0].indexOf('?') >= 0) {
                        var url2 = data[0] + "&tablesorter=" + val;
                    } else {
                        var url2 = data[0] + "?tablesorter=" + val;
                    }
                    location.href = url2;
                }
            }
        });
    }
}


//图片载入出错时载入默认图片
//占位图片
function default_img(o, w, h, t) {
    var t = arguments[3] ? arguments[3] : 'image';
    //http://placehold.it/宽x高/背景颜色/文字颜色.png&text=文字
    //e.g.
    //http://placehold.it/300x100/09f/fff.png&text=placehold.it+rocks!
    //文字使用不能使用中文
    o.src = img(w, h, t);
//	o.src = "http://placehold.it/" + w + "x" + h;
//	if(t){
//		o.src = o.src + "&text=" + t;
//	}
    $(o).removeAttr("onerror");
}

function img(w, h, t) {
    var t = arguments[2] ? arguments[2] : 'image';
//	var sr = "http://placehold.it/" + w + "x" + h + "&text=" + t;
//	if(t){
//		sr = sr + "&text=" + t;
//	}
    var sr = "http://www.atool.org/placeholder.png?size=" + w + "x" + h + "&text=" + t;
    return sr;
}

function default_img2(_this, img_src) {
    _this.src = img_src;
    $(_this).removeAttr("onerror");
}

//检查URL
function IsURL(str_url) {
//	var strRegex = '^((https|http|ftp|rtsp|mms){1}://)' 
//	+ '{1}(([0-9a-z_!~*\'().&=+$%-]+: )?[0-9a-z_!~*\'().&=+$%-]+@)?' //ftp的user@ 
//	+ '(([0-9]{1,3}.){3}[0-9]{1,3}' // IP形式的URL- 199.194.52.184 
//	+ '|' // 允许IP和DOMAIN（域名） 
//	+ '([0-9a-z_!~*\'()-]+.)*' // 域名- www. 
//	+ '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z].' // 二级域名 
//	+ '[a-z]{2,6})' // first level domain- .com or .museum 
//	+ '(:[0-9]{1,4})?' // 端口- :80 
//	+ '((/?)|' // a slash isn't required if there is no file name 
//	+ '(/[0-9a-z_!~*\'().;?:@&=+$,%#-]+)+/?)$'; 
    var strRegex = '^((http|https)://[0-9a-z_!~*\'().&=+$%-]+)|(\<domain_name\>/[0-9a-z_!~*\'().&=+$%-]+)';
    var re = new RegExp(strRegex);
    //re.test()
    if (re.test(str_url)) {
        return true;
    } else {
        return false;
    }
}

function IsURL1(str_url) {
    if (!IsURL(str_url)) {
        showMsg({msg: '请输入正确的网址', icon: 8});
        return false;
    }
}
/**
 * ajax请求方法
 * param: url 请求路径
 *        args 参数字典
 * */
function operate(url, args) {
    // 默认参数为空
    var data = args.data ? args.data : {}; //请求数据：默认为空
    var dataType = args.type ? args.type : 'text'; // 返回类型：默认为字符串
    var success = args.success ? args.success : function (data) {
    }; //请求成功回调
    $.ajax({
        type: "POST",
        url: url,
        async: false,
        cache: false,
        data: data,
        dataType: dataType,
        success: success,
        error: function () {
            showAlertMsg("链接服务器失败");
        },
    });
}

/***** 判断是否为json对象 *******
 * @param obj: 对象（可以是jq取到对象）
 * @return isjson: 是否是json对象 true/false
 */
function isJson(obj) {
    var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;
    return isjson;
}

/**
 * ajax公用方法
 *
 */
function ajaxRequest(url, args) {
    // 默认参数为空
    var data = args.data ? args.data : false;
    var success = args.success ? args.success : false;
    var type = args.type ? args.type : false;
    $.ajax({
        type: "GET",
        url: url,
        async: false,
        cache: false,
        data: data,
        success: success,
        dataType: type,
        error: function () {
            showAlertMsg("链接服务器失败");
        },
    });
}
