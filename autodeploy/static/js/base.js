$(function () {
    setTimeout('slideshowmsg()', 0);
    setTimeout('slidehidemsg()', 5000);
});

function slideshowmsg() {
    $(".messages").slideDown(500);
}

function slidehidemsg() {
    $(".messages").slideUp(500);
}

/*获取url中的域名或当前网址的域名*/
function getHost(url) {

    /*host1 = window.location.host;

     host2 = document.domain;*/

    var host = "null";
    if (typeof url == "undefined"
        || null == url)
        url = window.location.href;
    var regex = /.*\:\/\/([^\/]*).*/;
    var match = url.match(regex);
    if (typeof match != "undefined"
        && null != match)
        host = match[1];
    return host;
}

/*输入框验证方法*/
var flag = false;
//r正则表达式 e要验证的input元素
function verify(r, e) {
    //输入不为空
    if (e.val() != '') {
        //验证通过
        if (r.test(e.val())) {
            e.next().next().next().remove();
            e.next().next().next().remove();
            flag = true;
            //不通过
        } else {
            e.next().next().next().remove();
            e.next().next().next().remove();
            e.next().next().after("<br/><span class='maroon'>格式错误！请重新输入</span>");
            flag = false;
        }
    }
}

/*表单提交方法*/
function btnsubmit() {
    //提交前验证每个input框,验证不通过的break,之后nameFlag为false
    for (i = 0; i < $(".verify input").length; i++) {
        $(".verify input").eq(i).blur();
        if (!flag) {
            break;
        }
    }
    //只有flag为true时才能继续
    if (flag) {
        $("[type='submit']").attr("disabled", "disabled");
        showOnsubmit('正在保存...');
        return true;
    } else {
        return false;
    }
}

/*表单提交方法*/
function btnsubmit1() {
    //只有flag为true时才能继续
    $("[type='submit']").attr("disabled", "disabled");
    showOnsubmit('正在保存...');
    return true;
}
/*表单提交方法*/
//手机页面用
function btnsubmit2() {
    if (checkempty()) {
        $("#submitok").attr("disabled", "disabled");
        $('#form1').submit();
        return true;
    } else {
        return false;
    }

}

function checkempty() {
    price = sessionStorage.getItem('price');
    if (price == '') {
        return false
    } else {
        return true;
    }
}
