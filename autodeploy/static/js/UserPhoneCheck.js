$(function () {
    var count;
    var countdown;
    $("#getCode").val("获取验证码").removeAttr("disabled");
//	$("#id_mobilephone").removeAttr("readonly");

    $("#id_mobilephone").blur(function () {
        var phone = $("#id_mobilephone").val();
        CheckPhone(phone);
    });

    /* 点击获取验证码 */
    $("#getCode").click(function () {
        count = 60;
        /* $(this).attr("class","btn btn-default"); */
        var _this = this;
        var phone = $("#id_mobilephone").val();

        if (CheckPhone(phone)) {
            $(_this).val("短信发送中...");
            $("#id_mobilephone").attr("readonly", "readonly");
            //ajax发送请求验证码信息
            $.ajax({
                type: "GET",
                url: "/sms/getcode/",
                data: {'phone': $("#id_mobilephone").val()},
                cache: false,
                success: function (msg) {
                    /* var msg = eval(data); */
                    if (msg == '1') {
                        /* $("#getCode").val("发送成功"); */
                        $(_this).attr("disabled", true);
                        countdown = setInterval(function () {
                            CountDown(_this)
                        }, 1000);
                    } else if (msg == '-1100') {
                        $("#id_mobilephone").next().html('手机号格式不正确');
                        $(_this).val("获取验证码");
                    } else if (msg == '-1004') {
//						$(_this).val("请等待1分钟后重试");
                        alert("请等待1分钟后重试");
                        $(_this).val("获取验证码");
//						$(_this).attr("disabled",true)
                    } else if (msg == '-1000') {
                        alert("商家短信余额不足");
                        $(_this).val("获取验证码");
//						$(_this).attr("disabled",true)
                    } else if (msg == '-1003') {

//						$(_this).attr("disabled",true);
//						countdown = setInterval(function(){CountDown(_this,count)},1000);

                        alert("debug模式下不发送短信");
                        $(_this).val("获取验证码");
                        $(_this).attr("disabled", true)
                    } else {
//						$(_this).val("发送失败" + msg);
                        alert("发送失败" + msg);
                        $(_this).val("获取验证码");
                    }
                },
                error: function (msg) {
                    //请求出错处理
                    $("#getCode").val("请求失败" + msg);
                }
            });
        }
    });

    function CountDown(_this) {
        $(_this).val("发送成功(" + count + ")");
        if (count == 0) {
            $(_this).val("获取验证码").removeAttr("disabled");
            clearInterval(countdown);
        }
        count--;
    }
});


function CheckPhone(phone) {
    var r = /^0?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/;
    if (phone == '') {
        $("#id_mobilephone").next().html('手机号不能为空');
        $("[type='submit']").attr('disable', 'disable')
        return false;
    } else if (!r.test(phone)) {
        $("#id_mobilephone").next().html('手机号格式不正确');
        $("[type='submit']").attr('disable', 'disable')
        return false;
    } else if (r.test(phone)) {
        $("#id_mobilephone").next().html('');
        $("[type='submit']").removeAttr('disable')
        return true;
    }
    return false;
}


//验证手机号是否在地址表里存在
function checkPhoneExist() {
    var v = CheckPhone($("#id_mobilephone").val());
    if (v) {
        $.ajax({
            type: "POST",
            url: "/user_address/checkPhoneExist/",
            data: {'phone': $("#id_mobilephone").val()},
            async: false,
            cache: false,
            success: function (msg) {
                if (msg == '1001') {
                    //存在手机号的不验证
//	 				$("#getCode").hide();
//	 				$("#id_verifycode").hide();
                    $("#phonecode").hide();
                    $("#id_verifycode").removeAttr("required");
                } else if (msg == '1002') {
                    //请求出错处理,不存在的手机号,需验证
//	 				$("#getCode").show();
//	 				$("#id_verifycode").show();
                    $("#phonecode").show();
                    $("#id_verifycode").attr("required", "required");
                } else if (msg == '0') {
                    //请求出错处理,不存在的手机号,需验证
//	 				$("#getCode").hide();
//	 				$("#id_verifycode").hide();
                    $("#phonecode").hide();
                    $("#id_verifycode").removeAttr("required");
                } else {
                    alert("手机号验证失败");
                }
            },
            error: function (msg) {
                //请求出错处理,返回false
                alert("请求出错");
            }
        });
    }
}