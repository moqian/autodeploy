function createmenu(wid) {
    $.ajax({
        type: "POST",
        url: "/restaurant_info/wechat/createmenu/",
        data: {"wid": wid},
        cache: false,
        datatype: 'json',
        success: function (msg) {
            if (msg == '0') {
                showAlertMsg("生成成功,由于微信客户端缓存，需要24小时微信客户端才会生效.", 9);
            } else if (msg == '-2001') {
                showAlertMsg("什么功能都没开,不能生成微信菜单(" + msg + ")");
            } else {
                showAlertMsg("生成失败(" + msg + ").");
            }
        },
        error: function (msg) {
            showAlertMsg("无法链接服务器.");
        }
    });
}
function deletemenu(wid) {
    showMenuConfirm('你确认要撤消微信菜单吗?', wid)
}
function ajaxDeleteMenu(wid, index) {
    layer.close(index)
    $.ajax({
        type: "POST",
        url: "/restaurant_info/wechat/deletemenu/",
        data: {"wid": wid},
        cache: false,
        datatype: 'json',
        success: function (msg) {
            if (msg == 0) {
                showAlertMsg("撤消成功,由于微信客户端缓存，需要24小时微信客户端才会生效.", 9);
            } else {
                showAlertMsg("撤消失败(" + msg + ")");
            }
        },
        error: function (msg) {
            showAlertMsg("无法链接服务器.");
        }
    });
}
function showMenuConfirm(msg, wid) {
    var index = layer.confirm(msg, function () {
        ajaxDeleteMenu(wid, index)
    });
//	var d = dialog({
//	    title: '提示',
//	    content: msg,
//	    width: 220,
//	    button:[{value: '确定',
//			callback: function(){
//				ajaxDeleteMenu(wid)
//        	},
//        autofocus: true
//        },
//        {value: '取消',
//            callback: function(){
//                 
//             }
//       }]
//	});
//	d.showModal();
}
