# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-24 下午1:50.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging
import os
from autodeploy import settings

from celery import Celery, platforms

logger = logging.getLogger(__name__)
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'autodeploy.settings')
app = Celery('autodeploy', broker='redis://127.0.0.1:6379')
# export PYTHONOPTIMIZE=1
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
platforms.C_FORCE_ROOT = True
# @app.task(bind=True)
# def debug_task(self):
#     print('Request: {0!r}'.format(self.request))
from celery.signals import worker_process_init


@worker_process_init.connect
def fix_multiprocessing(**kwargs):
    from multiprocessing import current_process
    try:
        current_process()._config
    except AttributeError:
        current_process()._config = {'semprefix': '/mp'}
