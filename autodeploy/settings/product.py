# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-24 下午4:49.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)
from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

STATIC_ROOT = ''
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'static'),
)
# 最后[/]不写 网站域名, 微信网页端调用授权,支付宝退款
domain_name = ''
# mysql安装路径,备份时用
# MYSQL_PATH = 'd:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\'
# mysql备份保存路径
# MYSQL_BACKUP_PATH = 'd:/mall_mysql_backup/'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'autodeploy',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': '123456',
        # Empty for localhost through domain sockets or '127.0.0.1' for
        # localhost through TCP.
        'HOST': 'localhost',
        'PORT': '3306',  # Set to empty string for default.
        'ATOMIC_REQUESTS': True,  # 数据库回滚
    }
}
# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
#         'LOCATION': 'D:/auction_cache',
#         'TIMEOUT': 60,
#         'OPTIONS': {
#             'MAX_ENTRIES': 1000
#         }
#     }
# }

# 日志配置
LOGGING_PREFIX = 'local'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(threadName)s:%(thread)d] [%(name)s:%(lineno)d] [%(levelname)s]- %(message)s'
        },
    },
    'filters': {
    },
    'handlers': {
        #         'mail_admins': {
        #             'level': 'ERROR',
        #             'class': 'django.utils.log.AdminEmailHandler',
        #             'include_html': True,
        #         },
        'default': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            # 日志输出文件
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_all.log'),
            'maxBytes': 1024 * 1024 * 50,  # 文件大小
            'backupCount': 100,  # 备份份数
            'formatter': 'standard',  # 使用哪种formatters日志格式
        },
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_error.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'request_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_request.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        },
        'scripts_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_script.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['scripts_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'django.request': {
            'handlers': ['request_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'scripts': {
            'handlers': ['scripts_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        # 这写的应该是项目下的包名,而不是项目名
        'mall': {
            'handlers': ['default', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}
# 微信公众号参数
# WECHATCONFIG = {
#     'appid': '',  # 公众号id
#     'appsecret': '',  # 公众号密钥
#     'api_key': '',  # 支付密钥
#     'mch_id': '',  # 商户号

# }
# # 短信发送配置参数
# LEAN_CLOUD = {
#     # 'APP_ID': '5dYE68zvIHYFw4Fri6Y3tnLq-gzGzoHsz',
#     'APP_ID': 'Vn5YxjeYOUdTo42u37E9q64L-gzGzoHsz',
#     # 'APP_ID': '6SaNkNNY58NmAFQjyk0eJUah-gzGzoHsz',
#     # 'APP_KEY': '3WtcPvRFQVeCr7ShQ4va5e6S',
#     'APP_KEY': 'BO0FwNUHNNtkKlQ3WF8RcDhk',
#     # 'APP_KEY': 'xcAmCwP799nOSdViIfnbbU65',
#     'js': 'https://cdn1.lncld.net/static/js/av-min-1.2.1.js'
# }
# LOCAL_URL = "http://hcx.ngrok.io"
