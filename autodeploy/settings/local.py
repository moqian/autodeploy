# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-6-29 涓嬪崍3:14.
@author: 鑳¤秴缈�
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)
from .base import *
# celery 閰嶇疆
import djcelery

djcelery.setup_loader()
# BROKER_URL = 'django://'
# # BROKER_URL= 'redis://127.0.0.1:6379'
# # CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
# CELERY_ALWAYS_EAGER=True
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Shanghai'

DEBUG = False
ALLOWED_HOSTS = ['192.168.1.107','*']

STATIC_ROOT = os.path.join(PROJECT_PATH, "static/").replace('\\', '/')
#STATIC_ROOT=''
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'static'),
)
# 鏈�鍚嶽/]涓嶅啓 缃戠珯鍩熷悕, 寰俊缃戦〉绔皟鐢ㄦ巿鏉�,鏀粯瀹濋��娆�
domain_name = ''
# mysql瀹夎璺緞,澶囦唤鏃剁敤
# MYSQL_PATH = 'd:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\'
# mysql澶囦唤淇濆瓨璺緞
# MYSQL_BACKUP_PATH = 'd:/mall_mysql_backup/'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'autodeploy',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': '123456',
        # Empty for localhost through domain sockets or '127.0.0.1' for
        # localhost through TCP.
        'HOST': 'localhost',
        'PORT': '3306',  # Set to empty string for default.
        'ATOMIC_REQUESTS': True,  # 鏁版嵁搴撳洖婊�
    }
}
# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
#         'LOCATION': 'D:/auction_cache',
#         'TIMEOUT': 60,
#         'OPTIONS': {
#             'MAX_ENTRIES': 1000
#         }
#     }
# }

# 鏃ュ織閰嶇疆
LOGGING_PREFIX = 'local'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(threadName)s:%(thread)d] [%(name)s:%(lineno)d] [%(levelname)s]- %(message)s'
        },
    },
    'filters': {
    },
    'handlers': {
        #         'mail_admins': {
        #             'level': 'ERROR',
        #             'class': 'django.utils.log.AdminEmailHandler',
        #             'include_html': True,
        #         },
        'default': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            # 鏃ュ織杈撳嚭鏂囦欢
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_all.log'),
            'maxBytes': 1024 * 1024 * 50,  # 鏂囦欢澶у皬
            'backupCount': 100,  # 澶囦唤浠芥暟
            'formatter': 'standard',  # 浣跨敤鍝formatters鏃ュ織鏍煎紡
        },
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_error.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'request_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_request.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        },
        'scripts_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR + '/log/',
                                     LOGGING_PREFIX + '_script.log'),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 100,
            'formatter': 'standard',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['scripts_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'django.request': {
            'handlers': ['request_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'scripts': {
            'handlers': ['scripts_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
        # 杩欏啓鐨勫簲璇ユ槸椤圭洰涓嬬殑鍖呭悕,鑰屼笉鏄」鐩悕
        'autodeploy': {
            'handlers': ['default', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}
# 寰俊鍏紬鍙峰弬鏁�
# WECHATCONFIG = {
#     'appid': '',  # 鍏紬鍙穒d
#     'appsecret': '',  # 鍏紬鍙峰瘑閽�
#     'api_key': '',  # 鏀粯瀵嗛挜
#     'mch_id': '',  # 鍟嗘埛鍙�

# }
# # 鐭俊鍙戦�侀厤缃弬鏁�
# LEAN_CLOUD = {
#     # 'APP_ID': '5dYE68zvIHYFw4Fri6Y3tnLq-gzGzoHsz',
#     'APP_ID': 'Vn5YxjeYOUdTo42u37E9q64L-gzGzoHsz',
#     # 'APP_ID': '6SaNkNNY58NmAFQjyk0eJUah-gzGzoHsz',
#     # 'APP_KEY': '3WtcPvRFQVeCr7ShQ4va5e6S',
#     'APP_KEY': 'BO0FwNUHNNtkKlQ3WF8RcDhk',
#     # 'APP_KEY': 'xcAmCwP799nOSdViIfnbbU65',
#     'js': 'https://cdn1.lncld.net/static/js/av-min-1.2.1.js'
# }
# LOCAL_URL = "http://hcx.ngrok.io"
