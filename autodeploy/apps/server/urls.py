# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 下午2:38.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.conf.urls import url

from autodeploy.apps.server.views import ListServerView, EditorServerView, DetailServerView, DeleteServerView, \
    DeletePlayBooksRelationView, ListServerManageView, EditorServerManageView, DetailServerManageView, \
    DeleteServerManageView, AutoCompleteManageListView

logger = logging.getLogger(__name__)
app_name = 'server'
urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'', include('autodeploy.apps.index.urls')),url(r'', include('autodeploy.apps.index.urls')),
    url(r'manage/list/$', ListServerManageView.as_view(), name='manage_list'),
    url(r'manage/editor/(?P<smid>\d{1,10})?/?$', EditorServerManageView.as_view(), name='manage_editor'),
    url(r'manage/detail/(?P<pk>\d{1,10})?/?$', DetailServerManageView.as_view(), name='manage_detail'),
    url(r'manage/delete/(?P<smid>\d{1,10})?/?$', DeleteServerManageView.as_view(), name='manage_delete'),

    url(r'list/$', ListServerView.as_view(), name='list'),
    url(r'editor/(?P<sid>\d{1,10})?/?$', EditorServerView.as_view(), name='editor'),
    url(r'autocomplete/$', AutoCompleteManageListView.as_view(), name='autocomplete'),
    url(r'detail/(?P<pk>\d{1,10})/$', DetailServerView.as_view(), name='detail'),
    url(r'delete/(?P<sid>\d{1,10})/$', DeleteServerView.as_view(), name='delete'),
    url(r'delete/relation/$', DeletePlayBooksRelationView.as_view(), name='delete_relation'),
]
