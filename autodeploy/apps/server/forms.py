# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 下午2:38.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django import forms
from django.forms import TextInput, Textarea

from autodeploy.apps.server.models import Server, ServerManage

logger = logging.getLogger(__name__)
class ServerManageForm(forms.ModelForm):
    class Meta:
        model=ServerManage
        fields=('name','content')
        widgets={
            'name': TextInput(attrs={'type': 'text', 'required': 'required',
                                   'class': 'form-control input-lg null',
                                   'placeholder': u'请输入项目名字'}),
            'content': Textarea(attrs={'type': 'text', 'required': 'required',
                                     'class': 'form-control input-lg null',
                                     'placeholder': u'请输入对该项目的描述'}),
        }

class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = ('server_manage','ip', 'port', 'ssh_user', 'ssh_password')
        widgets = {
            'server_manage':TextInput(attrs={}),
            'ip': TextInput(attrs={'type': 'text', 'required': 'required',
                                   'class': 'form-control input-lg null',
                                   'placeholder': u'请输入ip地址'}),
            'port': TextInput(attrs={'type': 'text', 'required': 'required',
                                     'class': 'form-control input-lg null','value':'22',
                                     'placeholder': u'请输入ip端口'}),
            'ssh_user': TextInput(attrs={'type': 'text', 'required': 'required',
                                         'class': 'form-control input-lg null',
                                         'placeholder': u'请输入ip的账户名'}),
            'ssh_password': TextInput(attrs={'type': 'text', 'required': 'required',
                                             'class': 'form-control input-lg null',
                                             'placeholder': u'请输入ip的账户密码'}),
        }
