# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 下午2:38.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.contrib import admin

from autodeploy.apps.server.models import Server

logger = logging.getLogger(__name__)
admin.site.register(Server)
