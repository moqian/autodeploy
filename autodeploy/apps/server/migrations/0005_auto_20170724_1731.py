# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-07-24 09:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('server', '0004_auto_20170724_1729'),
    ]
    
    operations = [
        migrations.AlterField(
            model_name='server',
            name='playbooks',
            field=models.ManyToManyField(blank=True, to='playbooks.PlayBooks', verbose_name='\u5267\u672c'),
        ),
    ]
