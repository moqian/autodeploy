# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 下午2:38.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.db import models

from autodeploy.apps.common.managers import DefaultManager
from autodeploy.apps.common.models import BaseModel
from autodeploy.apps.playbooks.models import PlayBooks

logger = logging.getLogger(__name__)

class ServerManage(BaseModel):
    """服务器管理"""
    name=models.CharField(verbose_name="项目名",max_length=20)
    content=models.CharField(verbose_name="描述",max_length=100)
    objects = DefaultManager()
    objt = models.Manager()
    class Meta:
        db_table = 'ServerManage'
        verbose_name = '项目管理'
        verbose_name_plural = '项目管理列表'
        ordering = ['-id']
    
    def __unicode__(self):
        return unicode(self.name)

class Server(BaseModel):
    """服务器节点"""
    SERVER_CHOICES = ((0, '停止'), (1, '启动'), (2, '启动失败'), (3, '启动中'))
    server_manage=models.ForeignKey(ServerManage,verbose_name='项目管理',null=True,on_delete=models.SET_NULL)
    ip = models.CharField(verbose_name='ip地址', max_length=50)
    port = models.CharField(verbose_name='端口号', max_length=50)
    ssh_user = models.CharField(verbose_name='节点账户', max_length=30)
    ssh_password = models.CharField(verbose_name='节点密码', max_length=128)
    cpu_type = models.CharField(verbose_name='cpu类型', max_length=100, blank=True)
    cpu_total = models.CharField(verbose_name='cpu内核', max_length=50, blank=True)
    os_type = models.CharField(verbose_name='操作系统类型', max_length=50, blank=True)
    ipv4 = models.CharField(verbose_name='ipv4地址', max_length=100, blank=True)
    disk_total = models.CharField(verbose_name='硬盘总量', max_length=1000, blank=True)
    disk_mount = models.CharField(verbose_name='硬盘挂载名及容量', max_length=1000, blank=True)
    host_name = models.CharField(verbose_name='服务器主机名', max_length=100, blank=True)
    os_kernel = models.CharField(verbose_name='操作系统内核', max_length=100, blank=True)
    status = models.IntegerField(verbose_name='服务器运行状态', choices=SERVER_CHOICES, default=0)
    playbooks = models.ManyToManyField(PlayBooks, verbose_name='剧本', blank=True)
    # process = models.CharField(verbose_name='进程信息', max_length=10000, blank=True)
    objects = DefaultManager()
    objt = models.Manager()
    
    class Meta:
        db_table = 'server'
        verbose_name = '服务器'
        verbose_name_plural = '服务器列表'
        ordering = ['-id']
    
    def __unicode__(self):
        return unicode(self.id)
