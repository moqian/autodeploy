# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-20 下午2:30.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django import forms

logger = logging.getLogger(__name__)


class LoginForm(forms.Form):
    username = forms.RegexField(label=u'用户名', widget=forms.TextInput(
        attrs={'placeholder': '请输入用户名',
               'autocomplete': 'off', 'required': 'required',
               'check-type': 'name required', 'maxlength': '50',
               'class': 'mui-input-clear mui-input'}),
                                regex=r'^[\w_@.+-]+$', help_text=('小于15个字符,含数字,不含中文'),
                                error_messages={'max_length': u'用户名超过限制长度', 'required': u'用户名不能为空',
                                                'invalid': u'大于5个字符,小于15个字符,含数字,不含中文'})
    password = forms.CharField(label=u'密码', widget=forms.TextInput(
        attrs={'placeholder': '请输入密码', 'onkeypress': 'EnterClick(event)', 'autocomplete': 'off',
               'required': 'required', 'class': 'mui-input-clear mui-input', 'onfocus': 'this.type="password"'}),
                               error_messages={'max_length': u'登录密码超过限制长度', 'required': u'登录密码不能为空',
                                               'invalid': u'登录密码格式不正确'})
