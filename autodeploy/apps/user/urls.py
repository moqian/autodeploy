# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 下午2:38.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

from django.conf.urls import url

from autodeploy.apps.user.views import LoginView, LogoutView

app_name = 'user'
urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'', include('autodeploy.apps.index.urls')),url(r'', include('autodeploy.apps.index.urls')),
    url(r'login/$', LoginView.as_view(), name='login'),
    url(r'logout/$', LogoutView.as_view(), name='logout'),
]
