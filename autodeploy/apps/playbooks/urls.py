# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-20 下午2:36.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.conf.urls import url

from autodeploy.apps.playbooks.views import DetailPlayBookView, ListPlayBooksView, EditorPlayBookView, PlayBooksView, \
    DeletePlayBooksView, ListBookFileView, EditorBookFileView, DeleteBookFileView, ReplayPlayBooksView, \
    VersionIsExistView

logger = logging.getLogger(__name__)
app_name = 'playbooks'
urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'', include('autodeploy.apps.index.urls')),url(r'', include('autodeploy.apps.index.urls')),
    url(r'file/list/$', ListBookFileView.as_view(), name='file_list'),
    url(r'list/$', ListPlayBooksView.as_view(), name='list'),
    url(r'file/editor/(?P<fid>\d{1,10})?/?$', EditorBookFileView.as_view(), name='file_editor'),
    url(r'editor/(?P<pbid>\d{1,10})?/?$', EditorPlayBookView.as_view(), name='editor'),
    
    url(r'detail/(?P<pk>\d{1,10})?/?$', DetailPlayBookView.as_view(), name='detail'),
    url(r'file/delete/(?P<fid>\d{1,10})?/?$', DeleteBookFileView.as_view(), name='file_delete'),
    url(r'delete/(?P<pbid>\d{1,10})?/?$', DeletePlayBooksView.as_view(), name='delete'),
    url(r'run/$', PlayBooksView.as_view(), name='run'),
    url(r'replay/$', ReplayPlayBooksView.as_view(), name='replay'),#日志重新运行剧本
    url(r'version/is_exist/$', VersionIsExistView.as_view(), name='version_is_exist'),#剧本版本是否存在

]
