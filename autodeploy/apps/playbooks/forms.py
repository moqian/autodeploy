# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-27 上午10:33.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django import forms
from django.forms import TextInput, Textarea

from autodeploy.apps.playbooks.models import PlayBooks, PlayBooksFile

logger = logging.getLogger(__name__)


class PlayBooksForm(forms.ModelForm):
    class Meta:
        model = PlayBooks
        fields = ('name', 'function_name',   )
        widgets = {
            'name': TextInput(attrs={'type': 'text', 'required': 'required',
                                     'class': 'form-control input-lg null',
                                     'placeholder': '请输入剧本名字'}),
            'function_name': Textarea(attrs={'type': 'text', 'required': 'required',
                                              'class': 'form-control input-lg null',
                                              'placeholder': '剧本描述'}),
            # 'books_type': TextInput(attrs={'type': 'text', 'required': 'required',
            #                                'class': 'form-control input-lg null',
            #                                'placeholder': '剧本类型'}),
            # 'system_type': TextInput(attrs={'type': 'text', 'required': 'required',
            #                                 'class': 'form-control input-lg null',
            #                                 'placeholder': '操作系统'}),
            # 'content': Textarea(attrs={'type': 'text', 'required': 'required',
            #                            'class': 'form-control input-lg null',
            #                            'placeholder': '剧本描述'}),
            # 'path': TextInput(attrs={'type': 'text',
            #                          'readOnly': 'readOnly',
            #                          'class': 'upload_file', 'upimginfo': 'image,0,0'}),
        }
        
class PlayBooksFileForm(forms.ModelForm):
    class Meta:
        model = PlayBooksFile
        fields = ('playbooks', 'content','version','path','upload_file')
        widgets = {
            # 'playbooks': Select(attrs={'required': 'required',
            #                          'class': 'form-control input-lg null',
            #                          }),
            'version': TextInput(attrs={'type': 'text', 'required': 'required',
                                              'class': 'form-control input-lg null',
                                              'placeholder': '版本'}),
            'content': Textarea(attrs={'type': 'text', 'required': 'required',
                                       'class': 'form-control input-lg null',
                                       'placeholder': '版本描述'}),
            'path': TextInput(attrs={'type': 'text',
                                     'readOnly': 'readOnly',
                                     'class': 'form-control input-lg null upload_file', 'upimginfo': 'image,0,0'}),
            'upload_file': TextInput(attrs={'type': 'text',
                                     'readOnly': 'readOnly',
                                     'class': 'form-control input-lg null upload_zip', 'upimginfo': 'image,0,0'}),
        }
