# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-11 07:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('playbooks', '0004_auto_20170811_1557'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlayBooksFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='\u521b\u5efa\u65f6\u95f4')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='\u66f4\u65b0\u65f6\u95f4')),
                ('delete_time', models.DateTimeField(blank=True, null=True, verbose_name='\u5220\u9664\u65f6\u95f4')),
                ('path', models.CharField(max_length=1000, verbose_name='\u5267\u672c\u8def\u5f84')),
                ('content', models.CharField(max_length=500, verbose_name='\u5267\u672c\u63cf\u8ff0')),
                ('version', models.CharField(max_length=200, verbose_name='\u5267\u672c\u7248\u672c')),
                ('playbooks', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='playbooks.PlayBooks', verbose_name='\u5267\u672c')),
            ],
            options={
                'db_table': 'play_books_file',
                'verbose_name': '\u5267\u672c\u6587\u4ef6',
                'verbose_name_plural': '\u5267\u672c\u6587\u4ef6\u5217\u8868',
            },
        ),
    ]
