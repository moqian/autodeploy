# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-20 下午2:36.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.db import models

from autodeploy.apps.common.managers import DefaultManager
from autodeploy.apps.common.models import BaseModel

logger = logging.getLogger(__name__)


class PlayBooks(BaseModel):
    """剧本"""
    name = models.CharField(verbose_name='剧本名字', max_length=100)
    function_name = models.CharField(verbose_name='剧本说明', max_length=100)
    # books_type = models.CharField(verbose_name='剧本类型', max_length=100)
    # system_type = models.CharField(verbose_name='系统类型', max_length=100)
    # content = models.CharField(verbose_name='描述', max_length=500)
    # path = models.CharField(verbose_name='剧本路径', max_length=1000)
    objects = DefaultManager()
    objt = models.Manager()
    
    class Meta:
        db_table = 'playbooks'
        verbose_name = '剧本管理'
        verbose_name_plural = '剧本管理列表'

    def __unicode__(self):
        return unicode(self.name)
    
class PlayBooksFile(BaseModel):
    playbooks = models.ForeignKey(PlayBooks, verbose_name='剧本管理')
    path = models.CharField(verbose_name='剧本', max_length=1000)
    content = models.CharField(verbose_name='剧本描述', max_length=500)
    version = models.CharField(verbose_name='剧本版本', max_length=200)
    upload_file=models.CharField(verbose_name="剧本所需文件",max_length=1000,null=True,blank=True)
    objects = DefaultManager()
    objt = models.Manager()

    class Meta:
        db_table = 'play_books_file'
        verbose_name = '剧本文件'
        verbose_name_plural = '剧本文件列表'
