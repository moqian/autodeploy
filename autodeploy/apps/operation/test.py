# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-8-30 下午8:19.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)
from websocket import create_connection
ws = create_connection("ws://192.168.000.000:8000/echo_once")
print "Sending 'Hello, World'..."
ws.send("Hello, World")
print "Sent"
print "Reeiving..."
result = ws.recv()
print "Received '%s'" % result
ws.close()