# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-21 下午2:42.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.db import models

from autodeploy.apps.common.managers import DefaultManager
from autodeploy.apps.common.models import BaseModel
from autodeploy.apps.playbooks.models import PlayBooks
from autodeploy.apps.server.models import Server

logger = logging.getLogger(__name__)


class OperationLog(BaseModel):
    RESULT_STATUS = (('success', '成功'), ('defeat', '失败'))
    play_books = models.ForeignKey(PlayBooks, verbose_name='剧本', null=True, on_delete=models.SET_NULL)
    books_name = models.CharField(verbose_name="剧本名", blank=True, max_length=100, null=True)
    # content = models.CharField(verbose_name='操作内容', max_length=100)
    server = models.ForeignKey(Server, verbose_name='服务器', null=True, on_delete=models.SET_NULL)
    server_ip = models.CharField(verbose_name="ip", max_length=100, blank=True, null=True)
    # result = models.CharField(verbose_name='返回结果', max_length=1000)
    version = models.CharField(verbose_name='剧本版本', max_length=100, blank=True)
    status = models.CharField(verbose_name='状态', max_length=20, blank=True, choices=RESULT_STATUS, default='success')
    end_time = models.DateTimeField(verbose_name='剧本结束时间', null=True)
    objects = DefaultManager()
    objt = models.Manager()
    
    class Meta:
        db_table = 'operation_log'
        verbose_name = '操作日志记录'
        verbose_name_plural = '操作日志记录表'
        ordering = ['-id']


class OperationDetail(BaseModel):
    operation_log = models.ForeignKey(OperationLog, verbose_name='剧本日志')
    result = models.CharField(verbose_name='返回结果', max_length=1000)
    status = models.CharField(verbose_name='结果状态', max_length=1000, blank=True)

    class Meta:
        db_table = 'operation_detail'
        verbose_name = '操作返回记录'
        verbose_name_plural = '操作返回记录表'
        ordering = ['-id']
