# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-21 下午2:42.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.conf.urls import url

from autodeploy.apps import operation
from autodeploy.apps.operation import views
from autodeploy.apps.operation.views import ListOperationView, DeleteOperationView, DetailOperationView, \
    get_operation_list

logger = logging.getLogger(__name__)
app_name = 'operation'
urlpatterns = [
    url(r'detail/(?P<pk>\d{1,10})/$', DetailOperationView.as_view(), name='detail'),
    url(r'list/$', ListOperationView.as_view(), name='list'),
    url(r'websocket/$',get_operation_list,name='sm_list'),
    url(r'delete/(?P<oid>\d{1,10})/$', DeleteOperationView.as_view(), name='delete'),
]
