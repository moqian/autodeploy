# -*- coding: utf-8 -*- 
"""
Created by hcx on 2017/5/23 10:15.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from enroll.apps.apply.models import Enroll

logger = logging.getLogger(__name__)


def get_detail_count(user, activity):
    """获得用户一共报名了几人"""
    detail_list = []
    enroll_list = Enroll.objects.filter(user=user, activity=activity)
    for enroll in enroll_list:
        enroll_detail_list = enroll.enrollmiddle_set.filter()
        for detail in enroll_detail_list:
            detail_list.append(detail)
    detail_count = len(detail_list)
    return detail_count
