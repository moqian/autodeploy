# -*- coding: utf-8 -*-
'''
Created on 2015年7月21日

@author: lpf
'''
import logging

from django.core.cache import cache
from django.utils.functional import SimpleLazyObject

logger = logging.getLogger(__name__)


class WechatMiddleware(object):
    '''查询用户分组信息并放入request里'''
    
    def process_view(self, request, view_func, view_args, view_kwargs):
        
        usergroup = cache.get('usergroup_%s' % request.user.id)
        if not usergroup:
            try:
                usergroup = request.user.groups.all()[0]
            except:
                pass
            else:
                cache.set('usergroup_%s' % request.user.id, SimpleLazyObject(lambda: usergroup),
                          60 * 60 * 24)
        request.usergroup = usergroup
        # request.session.set_expiry(5 * 60 * 60)
        # request.wechatconfig = WechatConfig.objs.get_or_new()
        return None

# class SocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
#     def process_exception(self, request, exception):
#         if hasattr(social_exceptions, 'AuthCanceled'):
#             return HttpResponse("I'm the Pony %s" % exception)
#         else:
#             raise exception
