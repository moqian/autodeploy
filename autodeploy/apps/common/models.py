# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import unicode_literals

from datetime import datetime

from django.db import models

from autodeploy.apps.common.managers import DefaultManager


class BaseModel(models.Model):
    """继承用的model"""
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    delete_time = models.DateTimeField(verbose_name='删除时间', null=True, blank=True)
    
    class Meta:
        abstract = True
    
    def update_delete(self):
        """更新为已删除"""
        if self.delete_time:
            # 已删除
            re = 2
        else:
            # 更新为删除
            self.delete_time = datetime.now()
            self.save(update_fields=['delete_time'])
            re = 1
        return re


# def save_area(name_list, request, area=None):
#     """
#         通用保存区域方法
#         :param name_list:页面input框name
#         :param area:Area
#         :param request:django request
#         :return area
#         :rtype Area
#     """
#     province = request.POST.get(name_list[0], '')
#     city = request.POST.get(name_list[1], '')
#     country = request.POST.get(name_list[2], '')
#     # 先判断存在area,更新
#     if area:
#         area.province = province
#         area.city = city
#         area.region = country
#         area.save()
#     else:
#         area = Area.objects.create(province=province, city=city, region=country)
#     return area


# class Area(models.Model):
#     """区域，包含省份，城市，地区信息"""
#
#     province = models.CharField('省份', max_length=20, help_text='省份')
#     city = models.CharField('城市', max_length=50, help_text='城市')
#     region = models.CharField('地区', max_length=50, help_text='地区')
#
#     class Meta:
#         db_table = 'area'
#         verbose_name = '地区'
#         verbose_name_plural = '地区'
#         default_permissions = ()
#
#     def __str__(self):
#         return '{}{}{}'.format(self.province, self.city, self.region)


class ApschedulerLog(BaseModel):
    """定时器任务记录
    在哪一点执行一次的任务记录(date型),每天几点执行的任务不记
    定点任务重启后,过期的不会执行,需有记录去执行
    """
    function = models.CharField(verbose_name="执行方法", max_length=20)
    args = models.CharField(verbose_name="参数", max_length=20)
    job_id = models.CharField(verbose_name="任务ID", max_length=50)
    run_time = models.DateTimeField(verbose_name="执行时间")
    is_finish = models.BooleanField(verbose_name="已执行", default=False)
    
    objects = DefaultManager()
    
    class Meta:
        db_table = 'apscheduler_log'
        verbose_name = '定时器任务记录'
        verbose_name_plural = '定时器任务记录列表'
        ordering = ['-id']
        default_permissions = ()
    
    def update_finish(self):
        """更新为已执行"""
        self.is_finish = True
        self.save(update_fields=['is_finish', 'update_time'])
    
    def update_run_time(self, run_time):
        """修改该任务的执行时间"""
        self.run_time = run_time
        self.is_finish = False
        self.save(update_fields=['run_time', 'is_finish', 'update_time'])
