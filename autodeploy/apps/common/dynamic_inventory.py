# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-17 上午11:03.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)


def inventory_str(group_name, hosts, user='root', password='111111', pro=None):
    """
    group_name:hosts组名;
    hosts：服务器列表;
    user:测试机用户名，例如root
    password: 测试机密码
    pro: 测试机端口
    """
    host_name = ""
    logger.info('****************生成host文件*************')
    for host in hosts:
        if pro and pro != '22':
            host_name = host_name + "\n%s ansible_ssh_port=%s ansible_ssh_user=%s ansible_ssh_pass='%s'" % (
                host, pro, user, password)
        else:
            host_name = host_name + "\n%s ansible_ssh_user=%s ansible_ssh_pass='%s'" % (
                host, user, password)
    
    hosts = """[%s]\n%s""" % (group_name, host_name)
    logger.info('*************生成结束：host文件内容:%s********' % host_name)
    return hosts
