# -*- coding: UTF-8 -*-
'''
Created on 2014年11月24日下午2:12:16

@author: 李鹏飞
'''
import logging

from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

logger = logging.getLogger(__name__)

'''
用来处理的上传图片。如果这个函数独立存在的话，它的request.user
是匿名用户，request.session也和当前登录的用户不同。简单的解决
方法是接传入user_id
'''


@csrf_exempt
def save_image(request):
    '''
                保存上传图片
                创建:2014年12月1日下午3:42:48 李鹏飞
                更新:2014年12月1日下午3:42:48 李鹏飞
        :param: folder:文件夹名
        :param: width:宽px
        :param: height:高px
        :return: 
        :rtype: 
    '''
    # get的跳转到添加图片页
    return render_to_response('upload_file/image/upload_img.html',
                              context_instance=RequestContext(request))
