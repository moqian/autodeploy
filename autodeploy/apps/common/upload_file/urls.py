# -*- coding: UTF-8 -*-
from django.conf.urls import url

from autodeploy.apps.common.upload_file.upload_file import uploadify_script
from .upload_image import save_image

# from .view import get_qiniu_domain
# from .view import token, get_qiniu_private_domain
app_name = 'upload'
urlpatterns = [
    # 图片上传
    
    # url(r'^upload_token/$', token, name='token'),
    # url(r'^qiniu/domain/$', get_qiniu_domain, name='qiniu-domain'),
    # url(r'^qiniu/private/domain/$', get_qiniu_private_domain, name='qiniu-private-domain'),
    
    url(r'^upload_image/$', save_image, name='save_image'),
    url(r'^upload_file/$', uploadify_script, name='save_file'),
    #     url(r'^delete_uploadfile/$', delete_uploadfile, name='delete_uploadfile'),
    #     url(r'^uploadify_script/', uploadify_script, name="uploadify_script"),
    #     url(r'^upload_media/$', save_media, name='save_media'),  # 上传语音/视频

]
