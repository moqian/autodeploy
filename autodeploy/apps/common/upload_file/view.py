# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from enroll import settings
from qiniu import Auth

__author__ = '杨国锋'


# 配置迁移至settings
# ACCESS_KEY = "-r1UxnBAisOkccEw-zepvY7vNnTqiLEBtHfmUrbi"
# SECRET_KEY = "RBkLXNohzc9YEwXQGh0g5Qm3Q-CeZveJUDetMOBm"
# BUCKET_KEY = 'wiicome'  # 七牛云端的上传存储空间名


def token(request):
    # wiicome服务器设置七牛上传token
    q = Auth(settings.QINIU_ACCESS_KEY, settings.QINIU_SECRET_KEY)
    # 上传策略仅指定空间名和上传后的文件名，其他参数仅为默认值
    token = q.upload_token(settings.QINIU_BUCKET_NAME)
    data = {"uptoken": token}
    return JsonResponse(data)


@login_required
def get_qiniu_domain(request):
    """获取七牛公开空间域名"""
    
    data = {'domain': settings.QINIU_BUCKET_DOMAIN}
    return JsonResponse(data)


@login_required
def get_qiniu_private_domain(request):
    """获取七牛私有空间域名"""
    
    data = {'domain': settings.QINIU_PRIVATE_BUCKET_DOMAIN}
    return JsonResponse(data)


def get_qiniu_dict():
    '''
                返回七牛的配置,token等
                创建:2016年2月3日下午4:50:40 李鹏飞
                更新:2016年2月3日下午4:50:40 李鹏飞
        :param: 
        :return: 
        :rtype: 
    '''
    # wiicome服务器设置七牛上传token
    q = Auth(settings.QINIU_ACCESS_KEY, settings.QINIU_SECRET_KEY)
    # 上传策略仅指定空间名和上传后的文件名，其他参数仅为默认值
    token = q.upload_token(settings.QINIU_BUCKET_NAME)
    domain = settings.QINIU_BUCKET_DOMAIN
    return {'token': token, 'domain': domain}
