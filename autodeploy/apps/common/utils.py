# -*- coding: UTF-8 -*-
'''
Created on 2015年7月22日上午9:01:39

@author: 邱斌良
'''

import ConfigParser
import logging
import random
import re
import string
import time
import urllib2
from os.path import os

from enroll import settings
from enroll.apps.wechat.models import Refund

logger = logging.getLogger(__name__)


def parse_int(txt, default=0):
    '''
                转换string到int,string转换报错的返回0 
                创建:2014年9月24日下午5:36:07 李鹏飞
                更新:2014年9月24日下午5:36:07 李鹏飞
        :param: txt:要转的字符串
        :param: default:默认返回
        :return: int
        :rtype: 
    '''
    try:
        return int(txt)
    except:
        return default


def getWebConf(section, option):
    '''读取网站配置文件'''
    cf = ConfigParser.RawConfigParser()
    f = os.path.join(settings.BASE_DIR, "web.conf").replace('\\', '/')
    cf.read(f)
    re = cf.get(section, option)
    return re


def getResponse(url):
    '''访问链接得到结果状态'''
    req = urllib2.Request(url)
    res_data = urllib2.urlopen(req, None, 3)
    res = res_data.read()
    error = res.split('<error>')[1].split('</error>')[0]
    return error


def GenRandomnum(length):
    '''生成随机字符串，只含数字，可以指定长度'''
    chars = string.digits
    # 得出的结果中字符会有重复的
    renstrnum = ''.join([random.choice(chars) for i in range(length)])
    # 得出的结果中字符不会有重复的
    #     renstr = ''.join(random.sample(chars, 15))#最大长度支持62
    return renstrnum


def GenRandomstr(length):
    '''生成随机字符串，包括大小写字母、数字，可以指定长度'''
    chars = string.ascii_letters + string.digits
    # 得出的结果中字符会有重复的
    renstr = ''.join([random.choice(chars) for i in range(length)])
    # 得出的结果中字符不会有重复的
    #     renstr = ''.join(random.sample(chars, 15))#最大长度支持62
    return renstr


def GenRandomLowerAndNum(length):
    '''生成随机字符串，包括小写字母、数字，可以指定长度'''
    chars = string.ascii_lowercase + string.digits
    # 得出的结果中字符会有重复的
    renstr = ''.join([random.choice(chars) for i in range(length)])
    # 得出的结果中字符不会有重复的
    #     renstr = ''.join(random.sample(chars, 15))#最大长度支持62
    return renstr


def removeFile(name):
    '''
                删除上传的文件,仅限upload里面的
                创建:2014年10月15日上午11:45:55 李鹏飞
                更新:2014年10月15日上午11:45:55 李鹏飞
        :param: name:图片路径(数据库里存的)如:/upload/replyimg/...
        :return: 
        :rtype: 
    '''
    try:
        os.remove(
            os.path.join(settings.MEDIA_ROOT, name.replace(settings.MEDIA_URL, '')))
    except WindowsError:
        logger.warning(u'删除文件[%s]失败' % name)


def check_mobile(request):
    '''判断访问网站来自mobile还是pc'''
    # if(ua.match(/MicroMessenger/i)=="micromessenger") { 微信浏览器
    userAgent = request.META.get('HTTP_USER_AGENT')
    #     userAgent = request.headers['User-Agent']
    #     print userAgent
    #     if userAgent.find('MicroMessenger') == -1:
    #         print 'not wx'#判断是不是微信浏览器
    #     else:
    #         print 'wx'
    
    # userAgent = env.get('HTTP_USER_AGENT')
    _long_matches = r'googlebot-mobile|android|avantgo|blackberry|blazer|elaine|hiptop|ip(' \
                    r'hone|od)|kindle|midp|mmp|mobile|o2|opera mini|palm( ' \
                    r'os)?|pda|plucker|pocket|psp|smartphone|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce; ' \
                    r'' \
                    r'' \
                    r'' \
                    r'' \
                    r'' \
                    r'' \
                    r'' \
                    r'' \
                    r'(iemobile|ppc)|xiino|maemo|fennec'
    _long_matches = re.compile(_long_matches, re.IGNORECASE)
    _short_matches = r'1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(' \
                     r'av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(' \
                     r'ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(' \
                     r'n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(' \
                     r'it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez(' \
                     r'[4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(' \
                     r'ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(' \
                     r'aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(' \
                     r't|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(' \
                     r'k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(' \
                     r'di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[' \
                     r'0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(' \
                     r'6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(' \
                     r'ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[' \
                     r'2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(' \
                     r'01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(' \
                     r'al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(' \
                     r'gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(' \
                     r'\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(' \
                     r'52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(' \
                     r'\-|2|g)|yas\-|your|zeto|zte\-'
    _short_matches = re.compile(_short_matches, re.IGNORECASE)
    
    if _long_matches.search(userAgent) != None:
        return True
    user_agent = userAgent[0:4]
    if _short_matches.search(user_agent) != None:
        return True
    return False


def get_orderid(uid):
    '''
                生成支付订单号码
      :param: uid：微信用户user_id
      :return: 生成订单号
      :rtype:
    '''
    from enroll.apps.apply.models import Enroll
    # 订单生成的时间戳
    timestr = str(int(time.time()))
    # 时间戳+4位随机数+用户id
    orderid = 'd' + str(uid) + timestr + GenRandomnum(4)
    is_exists = Enroll.objects.filter(enroll_id=orderid).exists()
    if is_exists:
        return get_orderid(uid)
    else:
        return orderid


def get_couponid():
    '''
                生成优惠券号码
      :param: uid：微信用户user_id
      :return: 生成优惠券号
      :rtype:
    '''
    from enroll.apps.coupon.models import Coupon
    # 订单生成的时间戳
    timestr = str(int(time.time()))
    # 时间戳+4位随机数+用户id
    couponid = GenRandomLowerAndNum(6)
    is_exists = Coupon.objects.filter(coupon_id=couponid).exists()
    if is_exists:
        return get_couponid()
    else:
        return couponid


def get_recordid(uid):
    '''
                生成消费记录表的订单号码
      :param: uid：微信用户user_id
      :return: 生成订单号
      :rtype:
    '''
    from enroll.apps.wechat.models import ConsumeRecord
    # 订单生成的时间戳
    timestr = str(int(time.time()))
    # 时间戳+4位随机数+用户id
    recordid = 'r' + str(uid) + timestr + GenRandomnum(4)
    is_exists = ConsumeRecord.objects.filter(recordid=recordid).exists()
    if is_exists:
        return get_recordid(uid)
    else:
        return recordid


def get_refundid(uid):
    '''
                生成退款订单号码
      :param: uid：微信用户user_id
      :return: 生成订单号
      :rtype:
    '''
    # 订单生成的时间戳
    timestr = str(int(time.time()))
    # 时间戳+4位随机数+用户id
    recordid = 'r' + str(uid) + timestr + GenRandomnum(4)
    is_exists = Refund.objects.filter(refund_id=recordid).exists()
    if is_exists:
        return get_recordid(uid)
    else:
        return recordid
