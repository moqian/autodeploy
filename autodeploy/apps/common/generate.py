# -*- coding: utf-8 -*- 
"""
Created by hcx on 2016/8/15 14:34.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging
import random
import string
from datetime import datetime

logger = logging.getLogger(__name__)


def random_str(length):
    """
       生成随机字符串，包括大小写字母、数字，可以指定长度
        python3中为string.ascii_letters,而python2下则可以使用string.letters和string.ascii_letters
        :param length: 生成的字符串长度
        :return: 随机字符串
        :trype: string
    """
    chars = string.ascii_lowercase + string.digits
    # 得出的结果中字符会有重复的
    renstr = ''.join([random.choice(chars) for i in range(length)])
    # 得出的结果中字符不会有重复的
    # renstr = ''.join(random.sample(chars, 15))#最大长度支持62
    return renstr


def random_num(length):
    """
       生成随机字符串，只含数字，可以指定长度
        python3中为string.ascii_letters,而python2下则可以使用string.letters和string.ascii_letters
        :param length: 生成的字符串长度
        :return: 随机字符串
        :trype: string
    """
    chars = string.digits
    # 得出的结果中字符会有重复的
    renstrnum = ''.join([random.choice(chars) for i in range(length)])
    # 得出的结果中字符不会有重复的
    # renstr = ''.join(random.sample(chars, 15))#最大长度支持62
    
    return renstrnum


ORDER_PREFIX = 'zw'


def _order_id(prefix):
    now = datetime.now()
    time_str = '{}{}{}{}'.format(prefix,
                                 now.strftime('%Y')[2:],
                                 now.strftime('%m%d%H%M%S'),
                                 now.strftime('%f')[:3])
    return '{}{}'.format(time_str, random_str(3))


def create_order_id():
    return _order_id(ORDER_PREFIX)
