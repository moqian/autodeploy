# -*- coding: utf-8 -*- 
"""
model的管理器
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.db import models

logger = logging.getLogger(__name__)


class DefaultManager(models.Manager):
    use_for_related_fields = True
    
    def get_queryset(self):
        """默认管理器过滤已删除的"""
        return super(DefaultManager, self).get_queryset().filter(delete_time__isnull=True)
