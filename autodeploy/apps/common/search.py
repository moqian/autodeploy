# -*- coding: utf-8 -*- 
"""
Created by lpf on 2016/12/15 16:47:25.
@author: 李鹏飞
"""
from __future__ import absolute_import
from __future__ import unicode_literals

from django.db.models import Q
from django.views.generic import ListView, DetailView


class SearchView(ListView):
    """搜索(管理后台使用)
    此view继承ListView
    子类要用搜索及筛选状态的继承此view
    name_list: 子类需加此属性
    """
    name_list = []  # 搜索字段
    
    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context[self.context_object_name] = self.search()
        return context
    
    def search(self):
        """原queryset上再加搜索"""
        return self.get_queryset().filter(self.get_filter_tuple()).filter(**self.get_status()).filter(self.get_time_tuple())
    
    def get_status(self):
        """可继承,万一status有提示要求"""
        status = self.request.GET.get('status', '')
        kwargs = {}
        if status:
            kwargs = {'status': status}
        return kwargs
    
    def get_filter_tuple(self):
        """不需继承"""
        search_content = self.request.GET.get('searchaction', '')
        filter_info = Q()
        for name in self.name_list:
            kwargs = {
                '{}__{}'.format(name, 'icontains'): search_content
            }
            filter_info = filter_info | Q(**kwargs)
        return filter_info
    
    def get_time_tuple(self):
        """如果有根据时间查询的话会调用"""
        start_time = self.request.GET.get('start_time', '')
        end_time = self.request.GET.get('end_time', '')
        filter_info = Q()
        if start_time or end_time:
            start = {
                "create_time__gte": start_time,
                
            }
            end = {
                "end_time__lte": end_time
            }
            filter_info = Q(**start) & Q(**end)
        return filter_info


class SearchDetailView(DetailView):
    name_list = []  # 搜索字段
    
    def get_context_data(self, **kwargs):
        context = super(SearchDetailView, self).get_context_data(**kwargs)
        context[self.context_object_name] = self.search()
        return context
    
    def search(self):
        """原queryset上再加搜索"""
        return self.get_queryset().filter(self.get_filter_tuple()).filter(**self.get_status())
    
    def get_status(self):
        """可继承,万一status有提示要求"""
        status = self.request.GET.get('status', '')
        kwargs = {}
        if status:
            kwargs = {'status': status}
        return kwargs
    
    def get_filter_tuple(self):
        """不需继承"""
        search_content = self.request.GET.get('searchaction', '')
        filter_info = Q()
        for name in self.name_list:
            kwargs = {
                '{}__{}'.format(name, 'icontains'): search_content
            }
            filter_info = filter_info | Q(**kwargs)
        return filter_info


def judge_status_show(request, status_tuple):
    """判断返回给页面时，查询的状态"""
    status = request.GET.get('status', '')
    try:
        status = int(status)
    except:
        status_show = "全部"
    else:
        status_show = status_tuple[status][1]
    return status_show
