# -*- coding: utf-8 -*- 
"""
Created by lpf on 2016/11/23 10:54:43.
@author: 李鹏飞
一些共用的类,方法,mixins
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)
