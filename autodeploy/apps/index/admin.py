# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-8-2 下午4:36.
@author: 胡超翔
"""
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from django.contrib import admin
from kombu.transport.django import models as kombu_models

logger = logging.getLogger(__name__)
admin.site.register(kombu_models.Message)