# -*- coding: utf-8 -*- 
"""
Created by hcx on 17-7-19 上午11:12.
@author: 胡超翔
"""
from django.conf.urls import url

from autodeploy.apps.index.views import IndexView

app_name = 'index'
urlpatterns = [
    url(r'$', IndexView.as_view(), name='index'),
]
